package Collection::SwitchUtils;
#================================================================--
# File Name    : SwitchUtils.pm
#
# Purpose      : The utility functions for the rube switch that 
#                   can be used by many threads
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=================================================================
$| = 1;
use warnings;
use strict;

my $rubePkt = Packet::RubeG->new();

# O(n) where n is number of sockets
sub readSocks {
    my $pkg = shift @_;
    my $ports = shift @_;
    for (my $port = SwitchConfig::SOCPORT; 
        $port < SwitchConfig::MAXPORT; 
        $port++) 
    {
        $ports->{$port}->read();
    }
}

# O(n) where n is number of sockets
sub writeSocks {
    my $pkg = shift @_;
    my $ports = shift @_;
    
    for (my $port = SwitchConfig::SOCPORT; 
        $port < SwitchConfig::MAXPORT; 
        $port++) 
    {
        $ports->{$port}->write();
    }
}

# O(n) where n is number of sockets
sub readUpLink {
    my $pkg = shift @_;
    my $ports = shift @_;

    $ports->{SwitchConfig::UPLINKPORT}->read();
}

# O(n) where n is number of sockets
sub writeUpLink {
    my $pkg = shift @_;
    my $ports = shift @_;

    $ports->{SwitchConfig::UPLINKPORT}->write();
}

sub fetch {
    my $pkg = shift @_;
    my $ports = shift @_;
    my $lines = shift @_;
    my $queue = shift @_;
    my $table = shift @_;

    # We fetch messages from sockets
    for (my $port = SwitchConfig::SOCPORT; 
        $port < SwitchConfig::MAXPORT; 
        $port++) 
    {
        my $raw = $lines->{$port}->dequeue_packet();
        if (defined($raw)){
            ${$queue}[0]->enqueue($port);
            ${$queue}[1]->enqueue($raw);
        }
    }
    
    # Fetch uplink socket
    my $ulRaw = $lines->{SwitchConfig::UPLINKPORT}->dequeue_packet();
    if (defined($ulRaw)) {
        ${$queue}[0]->enqueue(SwitchConfig::UPLINKPORT);
        ${$queue}[1]->enqueue($ulRaw);
    }
}

sub process {
    my $pkg = shift @_;
    my $ports = shift @_;
    my $lines = shift @_;
    my $queue = shift @_;
    my $table = shift @_;

    my $time = 0;

    # Then we send the message out to all sockets O(n)
    if (${$queue}[0]->get_siz() > 0 && ${$queue}[1]->get_siz() > 0) {
        my $portIn = ${$queue}[0]->dequeue();
        my $raw = ${$queue}[1]->dequeue();

        if (defined($raw) && $raw ne "") {
            $time = Collection::Utils->get_time();
            $rubePkt->decode($raw);
            my $mac = $rubePkt->get_dest_mac();
            my $portOut = $table->get_iface($mac);
            if (defined($portOut) && $ports->{$portOut}->connected()){
                $lines->{$portOut}->enqueue_packet($raw);
            }
            
            if (SwitchConfig::DEBUG) {
                Collection::Utils->print_debug("OUT: $time:\t$raw\n");
            }
        }
    }
}

sub check_switch_table {
    my $pkg = shift @_;
    my $table = shift @_;

    $table->check_time();
}

1;