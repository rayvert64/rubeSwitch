#!/usr/bin/perl
#================================================================--
# File Name    : RuthSwitch! 
#
# Purpose      : A switch using a cycle executive
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#=========================================================

use warnings;
use strict;

use POSIX;
use IO::Select;
use IO::Socket;
use Time::HiRes;
use Time::Local;

use threads;
use threads::shared;
use Thread::Queue;
use Thread::Semaphore;
use FileHandle;

use lib "./";
use Record::Switch;
use Table::SWITCH;
use SwitchConfig;
use Packet::RubeG;
use Collection::Line;
use Collection::Queue;
use Collection::Utils;
use Collection::SwitchUtils;
use Sockets::SocConC;
use Sockets::SocConS;

my $cycleSm = Thread::Semaphore->new(0);

# Uplink 
my $uplink = undef;

my %lines;
my %ports;

# Create proper amount of ports
my $port;
for ($port = SwitchConfig::SOCPORT; $port < SwitchConfig::MAXPORT; $port++) {
    $lines{$port} = Collection::Line->new();
    $ports{$port} = Sockets::SocConS->new(SwitchConfig::HOSTNAME, 
                                        $port,
                                        $lines{$port});
}

# Create uplink port
$lines{SwitchConfig::UPLINKPORT} = Collection::Line->new();
$ports{SwitchConfig::UPLINKPORT} = Sockets::SocConC->new(SwitchConfig::UPLINKHOSTNAME, 
                                    SwitchConfig::UPLINKPORT,
                                    $lines{SwitchConfig::UPLINKPORT});

# Queue
my @queue = (Collection::Queue->new(), Collection::Queue->new());

my $done :shared = 0;

local $SIG{INT} = sub { leaveScript(); };

my $debugFh = FileHandle->new();

local $SIG{ALRM} = sub {
    # If cycle did not finish before the the deadline issue a warning
    if ($done == 0) {
        print("\n\t!WARNING MISSED DEADLINE!\n");
        if (SwitchConfig::DEBUG) {
            Collection::Utils->print_debug("\n\t!WARNING MISSED DEADLINE!\n");
        }
    }
    #print(Collection::Utils->get_time()."\n");
    $cycleSm->up();
};

Time::HiRes::ualarm(500000, SwitchConfig::ALARMTIME);

# Create a thread so that we can use the alarms and semaphores
my $thread1 = threads->create( 
    sub {
        # Thread 'cancellation' signal handler
        $SIG{'TERM'} = sub { threads->exit(); };

        my $i = 0;
        my $table = Table::SWITCH->new(SwitchConfig::TABLE);

        while (1) {
            $done = 0;

            # READ (Period = 1 ticks)
            Collection::SwitchUtils->readSocks(\%ports);
            Collection::SwitchUtils->readUpLink(\%ports);
            
            # Fetch (Period = 1 tick)
            Collection::SwitchUtils->fetch(\%ports, 
                                        \%lines, 
                                        \@queue,
                                        $table
                                    );

            # Process (Period = 1 tick) 
            Collection::SwitchUtils->process(\%ports, 
                                        \%lines, 
                                        \@queue,
                                        $table
                                    );

            # WRITE (Period = 1 ticks)
            Collection::SwitchUtils->writeSocks(\%ports);
            Collection::SwitchUtils->writeUpLink(\%ports);

            $i++;
            $done = 1;

            # Block until timer "tick"
            $cycleSm->down();
        }
    }
);

sub leaveScript {
    my $ev = shift @_;

    $cycleSm->up(10000000);

    $thread1->kill("TERM")->join();

    print("\nShut it down, shut it all down!!!!! \n");
    if ( defined($ev) ) {
        exit($ev);
    }
    else {
        exit(0);
    }
}

while (1) {
    wait();
}

1;
