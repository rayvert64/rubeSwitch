package Table::SWITCH;
#================================================================--
# File Name    : Table/SWITCH.pm
#
# Purpose      : table of SWITCH records
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

sub new {
   my $class = shift @_;
   my $table = shift @_;

   my $self = {
      table => $table,
      max => 10
   };

   bless ($self, $class);
   return $self;
}

sub delete {
   my $self = shift @_;
   my $mac = shift @_; #primary key

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->delete"));
   }

   if (!exists($self->{table}{$mac})) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->delete"));
   }

   delete($self->{table}{$mac});

}

sub add {
   my $self = shift @_;
   my $mac = shift @_; #primary key
   my $iface = shift @_;

   if (!defined($mac) || (!defined($iface))) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->add"));
   }

   if (!exists($self->{table}{$mac})) {
      $self->{table}{$mac} = Record::Switch->new();
      $self->{table}{$mac}->set_iface($iface);
   }

   $self->{table}{$mac}->set_time(Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC));

}

sub get_keys {
   my $self = shift @_;
   return keys(%{$self->{table}});
}

sub get_time {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->get_time"));
   }

   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_time();
   } else {
      return undef;
   } 
}

sub get_iface {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->get_iface"));
   }

   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_iface();
   } else {
      return undef;
   } 
}

sub set_iface {
   my $self = shift @_;
   my $mac = shift @_;
   my $iface = shift @_;

   if (!defined($mac) || (!defined($iface)) ||  (!exists($self->{table}{$mac})) ) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->set_dev"));
   }


   $self->{table}{$mac}->set_iface($iface);
}

sub set_time {
   my $self = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) ||  (!exists($self->{table}{$mac})) ) {
      die(Tosf::Exception::Trap->new(name => "Table::SWITCH->set_time"));
   }


   $self->{table}{$mac}->set_time($t);
}

sub check_time {
    my $self = shift @_;

    my $now = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);

    my $key;

    foreach $key (keys(%{$self->{table}})) {
        if ($now - $self->{table}{$key}->get_time() >= $self->{max}) {
            $self->delete($key);
        }
    }
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%{$self->{table}})) {
      $s = $s . "Mac: $key ";
      $s = $s . $self->{table}{$key}->dumps() . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%{$self->{table}})) {
      print ("Mac: $key \n");
      $self->{table}{$key}->dump();
      print ("\n");
   } 
}

1;
