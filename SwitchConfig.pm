package SwitchConfig;
#================================================================--
# File Name    : config.pm
#
# Purpose      : implements Switch Settings
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant DEBUG => 0;
use constant LOCALTIME => my $localtime = localtime();
use constant DEBUGFILENAME => 'log/debug/SwitchDebug'.POSIX::strftime("_%m-%d-%y\_%H-%M", 
                                    localtime()
                                    ).".txt";
use constant ALARMTIME => 2_500;
use constant UPLINKHOSTNAME => "localhost";
use constant UPLINKPORT => 42069;
use constant HOSTNAME => "localhost";
use constant SOCPORT => 42071;
use constant IFACEPOLLFREQ => 0.05;
use constant NUMPORTS => 80;
use constant MAXPORT => SOCPORT+NUMPORTS;
use constant TABLE => {
    0 => Record::Switch->new(42069),
    1 => Record::Switch->new(42071),
    2 => Record::Switch->new(42072),
    3 => Record::Switch->new(42073),
    4 => Record::Switch->new(42074),
    5 => Record::Switch->new(42075),
    6 => Record::Switch->new(42076),
    7 => Record::Switch->new(42077),
    8 => Record::Switch->new(42078),
    9 => Record::Switch->new(42079),
    10 => Record::Switch->new(42080),
    11 => Record::Switch->new(42081),
    12 => Record::Switch->new(42082),
    13 => Record::Switch->new(42083),
    14 => Record::Switch->new(42084),
    15 => Record::Switch->new(42085),
    16 => Record::Switch->new(42086),
    17 => Record::Switch->new(42087),
    18 => Record::Switch->new(42088),
    19 => Record::Switch->new(42089),
    20 => Record::Switch->new(42090),
    21 => Record::Switch->new(42091),
    22 => Record::Switch->new(42092),
    23 => Record::Switch->new(42093),
    24 => Record::Switch->new(42094),
    25 => Record::Switch->new(42095),
    26 => Record::Switch->new(42096),
    27 => Record::Switch->new(42097),
    28 => Record::Switch->new(42098),
    29 => Record::Switch->new(42099),
    30 => Record::Switch->new(42100),
    31 => Record::Switch->new(42101),
    32 => Record::Switch->new(42102),
    33 => Record::Switch->new(42103),
    34 => Record::Switch->new(42104),
    35 => Record::Switch->new(42105),
    36 => Record::Switch->new(42106),
    37 => Record::Switch->new(42107),
    38 => Record::Switch->new(42108),
    39 => Record::Switch->new(42109),
    40 => Record::Switch->new(42110),
    41 => Record::Switch->new(42111),
    42 => Record::Switch->new(42112),
    43 => Record::Switch->new(42113),
    44 => Record::Switch->new(42114),
    45 => Record::Switch->new(42115),
    46 => Record::Switch->new(42116),
    47 => Record::Switch->new(42117),
    48 => Record::Switch->new(42118),
    49 => Record::Switch->new(42119),
    50 => Record::Switch->new(42120),
    51 => Record::Switch->new(42121),
    52 => Record::Switch->new(42122),
    53 => Record::Switch->new(42123),
    54 => Record::Switch->new(42124),
    55 => Record::Switch->new(42125),
    56 => Record::Switch->new(42126),
    57 => Record::Switch->new(42127),
    58 => Record::Switch->new(42128),
    59 => Record::Switch->new(42129),
    60 => Record::Switch->new(42130),
    61 => Record::Switch->new(42131),
    62 => Record::Switch->new(42132),
    63 => Record::Switch->new(42133),
    64 => Record::Switch->new(42134),
    65 => Record::Switch->new(42135),
    66 => Record::Switch->new(42136),
    67 => Record::Switch->new(42137),
    68 => Record::Switch->new(42138),
    69 => Record::Switch->new(42139),
    70 => Record::Switch->new(42140),
    71 => Record::Switch->new(42141),
    72 => Record::Switch->new(42142),
    73 => Record::Switch->new(42143),
    74 => Record::Switch->new(42144),
    75 => Record::Switch->new(42145),
    76 => Record::Switch->new(42146),
    77 => Record::Switch->new(42147),
    78 => Record::Switch->new(42148),
    79 => Record::Switch->new(42149),
    80 => Record::Switch->new(42150)
};


1;
