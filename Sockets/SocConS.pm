package Sockets::SocConS;
#================================================================--
# File Name    : Sockets::SocConS 
#
# Purpose      : ServerSockets
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant MAXLEN => 100;
use constant HEARTBEAT => 'heartbeat';
use constant HEARTBEATPKT => '_G_heartbeat_H_';

my $debugFh = FileHandle->new();

sub new {
    my $class = shift @_;
    my $hostname = shift @_;
    my $port = shift @_;
    my $line = shift @_;

    my $self = {
        hostname => $hostname,
        port => $port,
        max => 0,
        counter => 0,
        buff => undef,
        inbuff => undef,
        soc => undef,
        newSoc => undef,
        select => IO::Select->new(),
        timeout => 15,
        timeoutTime => 0,
        heartBeatTimeout => 10,
        heartBeatTime => 0,
        line => $line,
        debugFh => $debugFh
    };

    # Here we try and open a socket on port 42069
    print("Try to open socket at host localhost / port ".$self->{port}.".\n");
    $self->{soc} = new IO::Socket::INET (
        LocalHost => '',
        LocalPort => $port,
        Proto => 'tcp',
        Reuse => 1,
        Listen => 0,
        Blocking => 0
    );

    if (!defined($self->{soc})) {
        print("Failed to open interface Socket \n");
        die("Socket undefined!");
    } else {
        print("Opened interface at host localhost / port $self->{port} \n");
        $self->{select}->add($self->{soc});
    }

    bless ($self, $class);
    return $self;
}

sub connect {
    my $self = shift @_;

    my @clients = $self->{select}->can_read(0);
    foreach my $fh (@clients) {
        if ($fh == $self->{soc}) {
            $self->{newSoc} = $self->{soc}->accept();
            $self->{select}->add($self->{newSoc});
        }
    }

    if (defined($self->{newSoc}) && $self->{newSoc}->connected()) {
        print("Accepted connection on interface ".$self->{port}."! \n");
        $self->{timeoutTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
        return 1;
    }

    return 0;
}

sub connected {
    my $self = shift @_;

    if (defined($self->{newSoc}) && $self->{newSoc}->connected()) {
        return 1;
    }

    return 0;
}

sub disconnect {
    my $self = shift @_;

    if (defined($self->{newSoc})) {
        if ($self->{select}->exists($self->{newSoc})) {
            $self->{select}->remove($self->{newSoc});
            print("Removed reference to interface from select \n");
        }
        print("Closing interface \n");

        $self->{line}->flush();

        close($self->{newSoc});
    }

    $self->{newSoc} = undef;
}

sub read {
    my $self = shift @_;

    if (defined($self->{newSoc})) {
        my $timeout = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC) -
                        $self->{timeoutTime};
        if ($self->{timeout} > $timeout && $self->{newSoc}->connected()) {
            $self->{buff} = undef;
            my @clients = $self->{select}->can_read(0);
            foreach my $fh (@clients) {
                if ($fh == $self->{newSoc}) {
                    $self->{newSoc}->recv($self->{buff}, MAXLEN);
                    last;
                }
            }

            if (defined($self->{buff}) && length($self->{buff}) != 0) {
                $self->{timeoutTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
                $self->{line}->enqueue_packet_fragment($self->{buff});
                my $peekPkt = $self->{line}->peek_pkt();
                if (defined($peekPkt)) {
                    my $time = Collection::Utils->get_time();
                    if (SwitchConfig::DEBUG) {
                        Collection::Utils->print_debug("IN: $time\t$peekPkt\n");
                    }
                }
            }
        } else {
            $self->disconnect();
        }
    } else {
        $self->connect();
    }
}

sub write {
    my $self = shift @_;

    if (defined($self->{newSoc})) {
        if ($self->{newSoc}->connected()) {
            $self->{buff} = $self->{line}->dequeue_packet_fragment(MAXLEN);

            if (defined($self->{buff})) {
                $self->{newSoc}->write($self->{buff});
            }

            my $timeout = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC) -
                            $self->{heartBeatTime};

            # After 10 secs we send a heartbeats
            if ($self->{heartBeatTimeout} < $timeout && defined($self->{newSoc})) {
                $self->{heartBeatTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
                $self->{buff} = $self->{line}->enqueue_packet(HEARTBEAT);
            }
        } else {
            $self->disconnect();
        }
    } else {
        #print("Connect".$self->{port}."\n");
        $self->connect();
    }
}

1;